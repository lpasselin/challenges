# Puzzle Heroes

## Face Mash

Pour chaque photo dans ce répertoire, vous devez trouver quels sont les personnages de la culture populaire qui ont été mélangés pour donner ces magnifiques portraits.
* Il y a toujours DEUX personnages par photo.
* Pour avoir le point vous pouvez inscrire jusqu'à trois noms de personnages pour essayer de tomber sur les deux bons. Vous avez donc droit à une erreur par photo.

Écrire les réponses dans ce README!

#### Réponses:

###### Photo 1
![](photo 1.png)

votre réponse: Daryl de walking dead avec Anakin

###### Photo 2
![](photo 2.png)

votre réponse: Sasuke et Ron Geremy

###### Photo 3
![](photo 3.png)

votre réponse: Tobias Funke et Skyler White

###### Photo
![](photo 4.png)

votre réponse: Mr Poopy Buthole et Ned Flanders

###### Photo 5
![](photo 5.png)

votre réponse: Khal Drogo et Keemstar

###### Photo 6
![](photo 6.png)

votre réponse: Wilson et oompa loompa ? jackie chan?

###### Photo 7
![](photo 7.png)

votre réponse: Glados et Ginette Reno

###### Photo 8
![](photo 8.png)

votre réponse: Starfox et Dustin de Stranger Things

###### Photo 9
![](photo 9.png)

votre réponse: Clint Eastwood et Thomas le ptit train (ou Ness?)

###### Photo 10
![](photo 10.png)

votre réponse: The One True God (Nicolas Cage) et Dr. House
