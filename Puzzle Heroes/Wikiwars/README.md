# Puzzle Heroes

## Wikiwars
Dans ce défi, vous devez trouver un chemin entre deux pages. Il faut ensuite que vous donniez la liste des pages parcourues. Le défi est considéré comme réussi, si un chemin est donné et valide pour chaque pages demandées.

### Chemins demandés

http://degreesofwikipedia.com/

Computer science → Willy Wonka & the Chocolate Factory

Matt Groening  → Brigitte Bardot

Tim Berners-Lee → Bob Ross

James Gosling → Damn Daniel

Széchenyi thermal bath → Joé Juneau

Time travel → Satoshi Nakamoto


Vous devez donner le nom des pages parcourues sous chacun des chemins.