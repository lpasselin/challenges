

def stringReplace(s, newChar, index):
    # remplace char a index
    return s[:index] + newChar + s[index+1:]

def sword_decorator(f):
    def wrap():
        s = f()
        s = s[:17] + '  }' + s[17:]
        s = s[:30] + '|}' + s[30:]
        s = s[:40] + '  }' + s[40:]
        return s
    return wrap

def shield_decorator(f):
    def wrap():
        s = f()
        s = '\\' + s
        s = stringReplace(s, '\\', 3)
        s = stringReplace(s, '_', 11)
        s = stringReplace(s, '\\', 12)
        s = stringReplace(s, '|', 13)
        s = stringReplace(s, 'M', 21)
        return s
    return wrap

@sword_decorator
@shield_decorator
def stickman():
    return open('stickman.txt').read()

if __name__ == "__main__":
    print(stickman())
    
    print('Essaie de switcher ordre des decorateurs lol')
