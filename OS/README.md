# OS

## Super quiz!

### Instruction
Vous devez répondre au questionnaire directement dans ce README en dessous de chaque question. Ces questions touchent plusieurs aspects de la programmation de bas niveau et les réponses sont généralement assez courtes.

###### Question 1
Quelle est la différence entre le heap et le stack (le monceau et la pile).

Heap pour allocation dynamique de variables (malloc)

Stack LIFO chaque pour chaque thread, utilisé gestion branchements de fonctions pour sauvegarde de contexte en cours.

###### Question 2
Que fais la fonction malloc?
Instruction d'allocation de mémoire (sur le heap) pour une nouvelle variable pendant exécution.

###### Question 3
Décrivez le fonctionnement de ce code.

```
uint16_t lastKeys = 0;
uint16_t diff, currentKeys;

while (1) {
	currentKeys = scanKeyboard();
	if ((diff = (uint16_t) (currentKeys & (~lastKeys))) || !currentKeys)
		lastKeys = currentKeys;
	faire_quelque_chose(diff);
}
```

boucle infinie qui fait quelque chose avec le diff des touches appuyées (ou pu appuyées) d'un clavier (diff entre la dernière itération des touches).

###### Question 4
Que signifie les 8 paramètres de cette commande GCC:  
`gcc -Os -Wall -g3 -flto -lpthread -o main main.c`

gnu c compiler

optimize for size

warnings

Request debugging information (level 3)

standard link-time optimizer

librairie lpthread (multithreading lib)

nom output (-o main)

main.c: fichier à compiler



###### Question 5
À quoi sert le DMA dans un microcontrolleur ou un ordinateur.
:)

Direct Memory Access: périphérique qui écrit et lit en mémoire ou dans un autre périphérique sans interruption de l'exection cpu. 
Exemple ADC fournit valeur à 50MHz: On configure DMA pour fetch la valeur et remplir un tableau en mémoire.
Cpu peut ensuite traiter en batch le tableau. Pas besoin d'interrompre son exec chaque 50MHz.


###### Question 6
Quelle est la différence entre mutex et sémaphore?

(utilisés pour gestion de concurrence)

Mutex est une resource qui peut être utilisée un à la fois. 

Sémaphore est plusieurs ressources disponibles plusieurs à la fois mais avec une limite maximale.


###### Question 7
Un microcontrolleur lit, avec son convertisseur analogique à numérique à 12 bits, la température d'une cuve de fermentation à l'aide d'un thermomètre. Votre microcontrolleur fonctionne en 5V. Le thermomètre retourne une valeur entre 0 et 5V correspondant aux températures respectivement entre 15-25 degrées Celcius. Assumez que le comportement du thermomètre est linéaire.
Donnez la précision de lecture de votre microcontrolleur. Dans quel type de variable devriez-vous sauvegarder la valeur lue de façon à conserver le maximum de précision (char, int, long, float, double, etc), pourquoi? S'il y a lieu, expliquez aussi à quoi correspondent les valeurs enregistrées. (Max 10 lignes)

ADC 12 bits max valeurs 2^12 valeurs possibles. 25-10deg dans 5-0V. Séparer 2^12 valeurs dans 5-0V. 
Chaque bit est 5V/(2^12)*(10deg/5V)
Sauvegarder dans 12 bits, peu importe le type, tant que c'est 12bits et plus on perd pas de précision. (uint16 ferait bien la job)


###### Question 8
Expliquer une façon de faire le "debounce" d'un bouton.


matériel: Ajouter un circuit RC pour réduire hautes fréquences. 

logiciel: Ajouter une machine d'état qui vérifie que le bouton est toujours pressé (ou pas) après un délai défini (ex 10ms est assez généralement pour un bouton mécanique)

###### Question 9
Comment pourriez-vous faire pour mesurer la capacitance (en Farrad) d'un condensateur à l'aide d'un Arduino ?

Choisir résistance connue et faire circuit RC avec le cap à mesurer. R*C = constante_de_temps en secondes.

Ajouter une switch avec un gpio et 5V ou GND (ou fournir 5V par gpio...)

Capter valeur aux bornes du cap avec un ADC. Cap atteint 63% de sa valeur après une durée équivalente à une constante_de_temps.

Déterminer constante_de_temps et résoudre constante_temps = R*C

C est farad.

Valider que arduino qui charge cap va pas faire chier l'ADC (ça va probablement le faire chier mais pas assez)


###### Question 10
Que fait ce code et à quoi peut-il servir ? Existe-t-il un équivalent dans la librairie standard du C?

`while (*p++ = *q++) ;`

copie q dans p

? jusqu'à ce que q pointe sur 0 



###### Question 11
Décrivez les principales fonctions d'un microprocesseur.

fetch decode execute

Fetch: get instruction from memory

decode: decode instructions et set les bits sur les bus de contrôle des périph

execute: execute instruction demandée avec l'alu ou avec les périphériques.